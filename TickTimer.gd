extends Timer

var GameId : int

func _on_TickTimer_timeout() -> void:
	Network.increase_tick(GameId)
