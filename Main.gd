extends Node2D

onready var TickTimer = preload("res://TickTimer.tscn")

func start_game(gameid : int) -> void:
	for child in $Timers.get_children():
		if child.GameId == gameid:
			child.start(Network.Games[gameid]["TickLength"])
	Network.unpause(gameid)

func create_tick_timer(gameid : int) -> void:
	var TickTimerInstance = TickTimer.instance()
	TickTimerInstance.GameId = gameid
	$Timers.add_child(TickTimerInstance)
