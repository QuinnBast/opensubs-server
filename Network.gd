extends Node

# Connection variables

var Port : int
var MaxConnectedClients : int

# Config variables

var Config := ConfigFile.new()
var VisibleTickMaxAhead : int
var CalculateTroopsDelay : float
var MaximumSubmarineArrivalTicks : int

# Game variables

var Games : Dictionary  = {}
var MaxPlayers : int
var TickLengths : Array
var Colors : Dictionary = {
		"Empty" : "4C4C4C",
		"Red" : "AB2024",
		"Orange" : "D28E2A",
		"Cyan" : "5C9A8C",
		"Sky" : "7BA7D8",
		"Blue" : "3A4BA3",
		"Purple" : "6A558E",
		"Pink" : "C777B1",
		"Olive" : "686E47",
		"Beige" : "9B907B",
		"Brown" : "8B5E3B"
	}

# Inbuilt functions

func _ready() -> void:
	randomize()
	create_config_file()
	load_config_file()
	print("Creating server")
	var Network := NetworkedMultiplayerENet.new()
	Network.create_server(Port, MaxConnectedClients)
	get_tree().set_network_peer(Network)
	Network.connect("peer_connected", self, "peer_connected")
	Network.connect("peer_disconnected", self, "peer_disconnected")

# Connection functions

# Called when peer connects to server
func peer_connected(id : int) -> void:
	print(str(id) + " connected")

# Called when peer disconnects from server
func peer_disconnected(id) -> void:
	print(str(id) + " disconnected")
	disconnect_player(id)

# Returns the IDs of all online players in a game
func get_online_player_ids(gameid : int) -> Array:
	var OnlinePlayers = Games[gameid]["OnlinePlayers"]
	var Ids : Array = []
	for player in OnlinePlayers:
		Ids.append_array(OnlinePlayers[player]["Ids"])
	return Ids

# Config file functions

# Returns the location of the config file
func get_config_path() -> String:
	return OS.get_executable_path().get_base_dir().plus_file("Server.cfg")

# Creates a config file if one doesn't exist
func create_config_file() -> void:
	if Config.load(get_config_path()) != OK:
		Config.set_value("Connection", "Port", 25565)
		Config.set_value("Connection", "MaxConnectedClients", 100)
		Config.set_value("Performance", "VisibleTickMaxAhead", 300)
		Config.set_value("Performance", "CalculateTroopsDelay", 0.5)
		Config.set_value("Performance", "MaximumSubmarineArrivalTicks", 1000)
		Config.set_value("GameSetup", "MaxPlayers", 10)
		Config.set_value("GameSetup", "TickLengths", [1, 10, 60, 600])
		Config.save(get_config_path())

# Loads the config file
func load_config_file() -> void:
	Config.load(get_config_path())
	Port = Config.get_value("Connection", "Port")
	MaxConnectedClients = Config.get_value("Connection", "MaxConnectedClients")
	VisibleTickMaxAhead = Config.get_value("Performance", "VisibleTickMaxAhead")
	CalculateTroopsDelay = Config.get_value("Performance", "CalculateTroopsDelay")
	MaximumSubmarineArrivalTicks = Config.get_value("Performance", "MaximumSubmarineArrivalTicks")
	MaxPlayers = Config.get_value("GameSetup", "MaxPlayers")
	TickLengths = Config.get_value("GameSetup", "TickLengths")

# Save functions

# Returns the location of the save file
func get_save_path() -> String:
	return OS.get_executable_path().get_base_dir().plus_file("Server.save")

# Loads the save file
func load_save_file() -> void:
	var SaveFile = File.new()
	SaveFile.open(get_save_path(), File.WRITE)

# Tick functions

# Increases the global tick by 1
func increase_tick(gameid : int) -> void:
	var GameData : Dictionary = Games[gameid]
	GameData["GlobalTick"] += 1
	var OldVisibleTickMax : int = GameData["VisibleTickMax"]
	GameData["VisibleTickMax"] = GameData["GlobalTick"] + VisibleTickMaxAhead
	set_history_expansion(gameid, OldVisibleTickMax, GameData["VisibleTickMax"])
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "set_global_variable", "GlobalTick", GameData["GlobalTick"])
		rpc_id(playerid, "update_visible_tick")

# Data functions

# TODO: call function less
# Returns the data for passed item
func get_data_name(gameid : int, itemname : String) -> Dictionary:
	var GameData : Dictionary = Games[gameid]
	for outpost in GameData["OutpostData"]:
		if outpost["OutpostName"] == itemname:
			return outpost
	for submarine in GameData["SubmarineData"]:
		if submarine["SubmarineName"] == itemname:
			return submarine
	# Failure case
	return {}

# Returns the index in history for passed tick
func get_tick_history(tick : int, history : Array) -> int:
	for i in len(history):
		if history[i]["Tick"] == tick:
			return i
	# Failure case
	return -1

# Returns the entry in history for passed tick
func get_tick_history_entry(tick : int, history : Array) -> Dictionary:
	var TickHistory = get_tick_history(tick, history)
	if TickHistory != -1:
		return history[TickHistory]
	else:
		# Failure case
		return {}

# Sets the passed outpost's history to passed value
func set_outpost_history(gameid : int, tick : int, outpostname : String, value : Array, overridetemporary : bool = true) -> void:
	var GameData = Games[gameid]
	var HistorySlice : Array = get_history_slice(tick, value)
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "set_outpost_history", tick, outpostname, HistorySlice, overridetemporary)

# Sets the passed submarine's history to passed value
func set_submarine_history(gameid : int, tick : int, submarinename : String, value : Array, overridetemporary : bool = true) -> void:
	var GameData = Games[gameid]
	var HistorySlice : Array = get_history_slice(tick, value)
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "set_submarine_history", tick, submarinename, HistorySlice, overridetemporary)

# Returns the passed history sliced to start at passed tick
func get_history_slice(tick : int, history : Array) -> Array:
	var TickHistory : int = get_tick_history(tick, history)
	return history.slice(TickHistory, -1)

# Creates a history for an outpost
func get_outpost_history(gameid : int, outpostname : String, initialplayer : String, initialtype : String, initialtroops : int, initialshield : int, initialshieldmax : int) -> Array:
	var GameData : Dictionary = Games[gameid]
	var History : Array = []
	for i in VisibleTickMaxAhead:
		History.append({
			"Tick" : GameData["GlobalTick"] + i,
			"Troops" : {outpostname : initialtroops},
			"TroopTotal" : initialtroops,
			"TroopTimeMultipliers" : {outpostname : 1},
			"TroopTimeMultiplierTotal" : 1,
			"TroopTimeCountdown" : GameData["GenerateTroopTicks"],
			"TroopNumberMultipliers" : {outpostname : GameData["GenerateTroopNumber"]},
			"TroopNumberMultiplierTotal" : 1,
			"Shield" : {outpostname : initialshield},
			"ShieldTotal" : initialshield,
			"ShieldTimeMultipliers" : {outpostname : 1},
			"ShieldTimeMultiplierTotal" : 1,
			"ShieldTimeCountdown" : GameData["GenerateShieldTicks"],
			"ShieldNumberMultipliers" : {outpostname : GameData["GenerateShieldNumber"]},
			"ShieldNumberMultiplierTotal" : 1,
			"ShieldMax" : initialshieldmax,
			"Players" : [{"Source" : outpostname, "Value" : initialplayer}],
			"CurrentPlayer" : initialplayer,
			"Types" : [{"Source" : outpostname, "Value" : initialtype}],
			"CurrentType" : initialtype
		})
	set_troop_generation(gameid, GameData["GlobalTick"], outpostname, History)
	set_shield_generation(gameid, GameData["GlobalTick"], outpostname, History)
	return History

# Creates a history for a submarine
func get_submarine_history(gameid : int, tick : int, submarinename : String, initialoutpost : String, initialplayer : String, initialposition : Vector2, initialtarget : String) -> Array:
	var History : Array = [{
		"Tick" : tick,
		"Preparing" : false,
		"Position" : initialposition,
		"Target" : initialtarget,
		"Troops" : {},
		"TroopTotal" : 0,
		"SpeedMultipliers" : {self : 1},
		"SpeedMultiplierTotal" : 1,
		"Gift" : false,
		"Players" : [{"Source" : self, "Value" : initialplayer}],
		"CurrentPlayer" : initialplayer
	}]
	set_submarine_positions(gameid, tick, submarinename, initialoutpost, History)
	return History

# Sets a submarine's positions
func set_submarine_positions(gameid : int, tick : int, submarinename : String, initialoutpost : String, submarinehistory : Array) -> void:
	var GameData : Dictionary = Games[gameid]
	var TickHistory : int = get_tick_history(tick, submarinehistory)
	var StartHistoryEntry : Dictionary = submarinehistory[TickHistory].duplicate(true)
	for i in MaximumSubmarineArrivalTicks:
		var ITickHistory : int = TickHistory + i
		if ITickHistory >= len(submarinehistory):
			var NewHistoryEntry : Dictionary = StartHistoryEntry.duplicate(true)
			NewHistoryEntry["Tick"] = StartHistoryEntry["Tick"] + i
			submarinehistory.append(NewHistoryEntry)
		var HistoryEntry : Dictionary = submarinehistory[ITickHistory]
		var OldPosition : Vector2 = submarinehistory[0]["Position"]
		if ITickHistory != 0:
			OldPosition = submarinehistory[ITickHistory - 1]["Position"]
		if HistoryEntry["Tick"] > submarinehistory[0]["Tick"] + GameData["SubmarineWaitModifier"]:
			var CurrentTargetPosition : Vector2 = get_game_position(gameid, HistoryEntry["Tick"], HistoryEntry["Target"])
			var CurrentDirectionAngle : float = OldPosition.angle_to_point(CurrentTargetPosition)
			var CurrentPosition : Vector2 = OldPosition + Vector2(GameData["SubmarineSpeed"] * HistoryEntry["SpeedMultiplierTotal"], 0).rotated(CurrentDirectionAngle + PI)
			var NextTargetPosition : Vector2 = get_game_position(gameid, HistoryEntry["Tick"], submarinehistory[ITickHistory]["Target"])
			var NextDirectionAngle : float = CurrentPosition.angle_to_point(NextTargetPosition)
			var NextPosition : Vector2 = CurrentPosition + Vector2(GameData["SubmarineSpeed"] * submarinehistory[ITickHistory]["SpeedMultiplierTotal"], 0).rotated(NextDirectionAngle + PI)
			HistoryEntry["Position"] = CurrentPosition
			if Rect2(CurrentPosition, Vector2()).expand(NextPosition).has_point(CurrentTargetPosition):
				var ToErase : Array = []
				for historyentry in submarinehistory:
					if historyentry["Tick"] >= HistoryEntry["Tick"]:
						ToErase.append(historyentry)
				for historyentry in ToErase:
					submarinehistory.erase(historyentry)
				break
		else:
			HistoryEntry["Preparing"] = true
			HistoryEntry["Position"] = OldPosition
	set_submarine_combat(gameid, submarinename, initialoutpost, submarinehistory)
	set_submarine_arrival(gameid, submarinename, initialoutpost, submarinehistory)

# Gets the passed item's position at passed tick
func get_game_position(gameid : int, tick : int, itemname : String) -> Vector2:
	var Data : Dictionary = get_data_name(gameid, itemname)
	if Data.has("SubmarineName"):
		var History : Array = Data["History"]
		return get_tick_history_entry(tick, History)["Position"]
	elif Data.has("OutpostName"):
		return Data["InitialPosition"]
	# Failure case
	return Vector2()

# Sets the history collection dictionary entry for source and calculates new total
func set_history_totals(tick : int, itemhistory : Array, collection : String, total : String, source : String, value : int) -> void:
	var TickHistory : int = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		itemhistory[ITickHistory][collection][source] = value
		var NewCollectionTotal : int = 0
		for collectionentry in itemhistory[ITickHistory][collection]:
			NewCollectionTotal += itemhistory[ITickHistory][collection][collectionentry]
		itemhistory[ITickHistory][total] = NewCollectionTotal

# Remove the history collection dictionary entry for source and calculates new total
func remove_history_totals(tick : int, itemhistory : Array, collection : String, total : String, source : String) -> void:
	var TickHistory : int = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		itemhistory[ITickHistory][collection].erase(source)
		var NewCollectionTotal : int = 0
		for collectionentry in itemhistory[ITickHistory][collection]:
			NewCollectionTotal += itemhistory[ITickHistory][collection][collectionentry]
		itemhistory[ITickHistory][total] = NewCollectionTotal

# Sets the history list array entry for source and calculates new current value
func set_history_current(tick : int, itemhistory : Array, list : String, current : String, source : String, value : String) -> void:
	var TickHistory : int = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		itemhistory[ITickHistory][list].insert(0, {"Source" : source, "Value" : value})
		itemhistory[ITickHistory][current] = itemhistory[ITickHistory][list][0]["Value"]

# Removes the history list array entry for source and calculates new current value
func remove_history_current(tick : int, itemhistory : Array, list : String, current : String, source : String) -> void:
	var TickHistory : int = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		var ITickHistory : int = TickHistory + i
		for secondaryi in len(itemhistory[ITickHistory][list]):
			if itemhistory[ITickHistory][list][secondaryi]["Source"] == source:
				itemhistory[ITickHistory][list].remove(secondaryi)
				break
		itemhistory[ITickHistory][current] = itemhistory[ITickHistory][list][0]["Value"]

# Sets the history boolean
func set_history_boolean(tick : int, itemhistory : Array, boolean : String, value : bool) -> void:
	var TickHistory : int = get_tick_history(tick, itemhistory)
	for i in len(itemhistory) - TickHistory:
		itemhistory[TickHistory + i][boolean] = value

# Calculates outpost history after attack
func set_outpost_attacked(gameid : int, tick : int, outpostname : String, outposthistory : Array, source : String, attacktroops : int, attackplayer : String) -> void:
	remove_outpost_troops(tick, outposthistory, source)
	remove_outpost_shield(tick, outposthistory, source)
	remove_outpost_player(tick, outposthistory, source)
	var TickHistory : int = get_tick_history(tick, outposthistory)
	var OverallShield : int = min(max(attacktroops, 0), outposthistory[TickHistory]["ShieldTotal"])
	var OverallTroops : int = attacktroops - OverallShield
	if  outposthistory[TickHistory]["TroopTotal"] - OverallTroops < 0:
		var RemainingTroops : int = -(outposthistory[TickHistory]["TroopTotal"] * 2) + OverallTroops
		set_outpost_troops(tick, outposthistory, source, RemainingTroops)
		set_outpost_player(tick, outposthistory, source, attackplayer)
	else:
		set_outpost_troops(tick, outposthistory, source, -OverallTroops)
	set_outpost_shield(tick, outposthistory, source, -OverallShield)
	set_troop_generation(gameid, tick, outpostname, outposthistory)
	set_shield_generation(gameid, tick, outpostname, outposthistory)
	set_outpost_history(gameid, tick, outpostname, outposthistory)

# Calculates outpost history after arrival
func set_outpost_arrival(gameid : int, tick : int, outpostname : String, outposthistory : Array, source : String, arrivaltroops : int, arrivalplayer : String, gift : bool) -> void:
	remove_outpost_troops(tick, outposthistory, source)
	remove_outpost_shield(tick, outposthistory, source)
	remove_outpost_player(tick, outposthistory, source)
	set_outpost_troops(tick, outposthistory, source, arrivaltroops)
	set_outpost_history(gameid, tick, outpostname, outposthistory)
	if gift == false:
		set_outpost_player(tick, outposthistory, source, arrivalplayer)

# Checks for sub-to-sub combat
func set_submarine_combat(gameid : int, submarinename : String, initialoutpost : String, submarinehistory : Array) -> void:
	var GameData : Dictionary = Games[gameid]
	for submarine in GameData["SubmarineData"]:
		if submarine["SubmarineName"] != submarinename:
			if submarinehistory[0]["Tick"] >= submarine["History"][0]["Tick"] or submarinehistory[-1]["Tick"] <= submarine["History"][-1]["Tick"]:
				for i in len(submarinehistory) - 1:
					if submarinehistory[i + 1]["Tick"] < submarine["History"][-1]["Tick"]:
						var CombatTick : int = submarinehistory[i]["Tick"]
						var ItemTickHistory : int = get_tick_history(CombatTick, submarine["History"])
						if ItemTickHistory != -1:
							if submarinehistory[i]["Target"] == get_data_name(gameid, submarine["SubmarineName"])["InitialOutpost"] and submarine["History"][ItemTickHistory]["Target"] == initialoutpost:
								if submarinehistory[i]["CurrentPlayer"] != submarine["History"][ItemTickHistory]["CurrentPlayer"]:
									var CurrentPosition : Vector2 = submarinehistory[i]["Position"]
									var NextPostion : Vector2 = submarinehistory[i + 1]["Position"]
									var CurrentItemPosition : Vector2 = submarine["History"][ItemTickHistory]["Position"]
									var NextItemPosition : Vector2 = submarine["History"][ItemTickHistory + 1]["Position"]
									var CombatRect : Rect2 = Rect2(CurrentPosition, Vector2()).expand(NextPostion)
									var ItemCombatRect : Rect2 = Rect2(CurrentItemPosition, Vector2()).expand(NextItemPosition)
									if CombatRect.intersects(ItemCombatRect):
										remove_submarine_attacked(CombatTick + 1, submarinehistory, submarine["SubmarineName"])
										remove_submarine_attacked(CombatTick + 1, submarine["History"], submarinename)
										var OldTroopTotal : int = submarinehistory[i]["TroopTotal"]
										set_submarine_attacked(gameid, CombatTick + 1, submarinename, initialoutpost, submarinehistory, submarine["SubmarineName"], submarine["History"][ItemTickHistory]["TroopTotal"], submarine["History"][ItemTickHistory]["CurrentPlayer"])
										set_submarine_attacked(gameid, CombatTick + 1, submarine["SubmarineName"], submarine["InitialOutpost"], submarine["History"], submarinename, OldTroopTotal, submarinehistory[i]["CurrentPlayer"])
										set_submarine_history(gameid, CombatTick + 1, submarine["SubmarineName"], submarine["History"])
							else:
								break
						else:
							break
					else:
						break

# TODO: call function less
# Check for submarines effected by arrival
func set_submarine_arrival(gameid : int, submarinename : String, initialoutpost : String, submarinehistory : Array) -> void:
	var GameData : Dictionary = Games[gameid]
	if submarinehistory[-1]["Tick"] <= GameData["VisibleTickMax"]:
		calculate_submarine_arrival(gameid, submarinename, submarinehistory)
		for submarine in GameData["SubmarineData"]:
			if submarine["SubmarineName"] != submarinename:
				if submarine["History"][-1]["Target"] == submarinehistory[-1]["Target"] or submarine["History"][-1]["Target"] == initialoutpost:
					if submarinehistory[-1]["Tick"] <= submarine["History"][-1]["Tick"]:
						calculate_submarine_arrival(gameid, submarine["SubmarineName"], submarine["History"])

# Calculates submarine history after arrival
func calculate_submarine_arrival(gameid : int, submarinename : String, submarinehistory : Array) -> void:
	var TargetData : Dictionary = get_data_name(gameid, submarinehistory[-1]["Target"])
	if TargetData.has("OutpostName"):
		var GameData : Dictionary = Games[gameid]
		var EndTick : int = submarinehistory[-1]["Tick"]
		var EndTickHistoryEntry : Dictionary = get_tick_history_entry(EndTick, TargetData["History"])
		if EndTickHistoryEntry["CurrentPlayer"] == submarinehistory[-1]["CurrentPlayer"] or (submarinehistory[-1]["Gift"] and GameData["Players"][EndTickHistoryEntry["CurrentPlayer"]]["Username"] != "Empty"):
			set_outpost_arrival(gameid, EndTick + 1, TargetData["OutpostName"], TargetData["History"], submarinename, submarinehistory[-1]["TroopTotal"], submarinehistory[-1]["CurrentPlayer"], submarinehistory[-1]["Gift"])
		else:
			set_outpost_attacked(gameid, EndTick + 1, TargetData["OutpostName"], TargetData["History"], submarinename, submarinehistory[-1]["TroopTotal"], submarinehistory[-1]["CurrentPlayer"])

# Calculates submarine history after attack
func set_submarine_attacked(gameid : int, tick : int, submarinename : String, initialoutpost : String, submarinehistory : Array, source : String, attacktroops : int, attackplayer : String) -> void:
	# TODO: implement sub change ownership
	var TickHistory : int = get_tick_history(tick, submarinehistory)
	if submarinehistory[TickHistory]["Troops"].has(source) == false or -attacktroops != submarinehistory[TickHistory]["Troops"][source]:
		var AttackTroopTotal : int = min(attacktroops, submarinehistory[TickHistory]["TroopTotal"])
		set_submarine_troops(tick, submarinehistory, source, -AttackTroopTotal)
	set_submarine_arrival(gameid, submarinename, initialoutpost, submarinehistory)

# Calculates submarine history after removal of attack
func remove_submarine_attacked(tick : int, submarinehistory : Array, source : String) -> void:
	remove_submarine_troops(tick, submarinehistory, source)

# Sets outpost troops
func set_outpost_troops(tick : int, outposthistory : Array, source : String, value : int) -> void:
	set_history_totals(tick, outposthistory, "Troops", "TroopTotal", source, value)

# Sets outpost shield
func set_outpost_shield(tick : int, outposthistory : Array, source : String, value : int) -> void:
	set_history_totals(tick, outposthistory, "Shield", "ShieldTotal", source, value)

# Sets outpost player
func set_outpost_player(tick : int, outposthistory : Array, source : String, value : String) -> void:
	set_history_current(tick, outposthistory, "Players", "CurrentPlayer", source, value)

# Sets submarine troops
func set_submarine_troops(tick : int, submarinehistory : Array, source : String, value : int) -> void:
	set_history_totals(tick, submarinehistory, "Troops", "TroopTotal", source, value)

# Sets submarine player
func set_submarine_player(tick : int, submarinehistory : Array, source : String, value : String) -> void:
	set_history_current(tick, submarinehistory, "Players", "CurrentPlayer", source, value)

# Sets submarine preparing
func set_submarine_preparing(tick : int, submarinehistory : Array, value : bool) -> void:
	set_history_boolean(tick, submarinehistory, "Preparing", value)

# Sets submarine gift
func set_submarine_gift(tick : int, submarinehistory : Array, value : bool) -> void:
	set_history_boolean(tick, submarinehistory, "Gift", value)

# Removes outpost troops
func remove_outpost_troops(tick : int, outposthistory : Array, source : String) -> void:
	remove_history_totals(tick, outposthistory, "Troops", "TroopTotal", source)

# Removes outpost shield
func remove_outpost_shield(tick : int, outposthistory : Array, source : String) -> void:
	remove_history_totals(tick, outposthistory, "Shield", "ShieldTotal", source)

# Removes outpost player
func remove_outpost_player(tick : int, outposthistory : Array, source : String) -> void:
	remove_history_current(tick, outposthistory, "Players", "CurrentPlayer", source)

# Removes submarine troops
func remove_submarine_troops(tick : int, submarinehistory : Array, source : String) -> void:
	remove_history_totals(tick, submarinehistory, "Troops", "TroopTotal", source)

# Removes submarine player
func remove_submarine_player(tick : int, submarinehistory : Array, source : String) -> void:
	remove_history_current(tick, submarinehistory, "Players", "CurrentPlayer", source)

# Removes outpost submarine changes
func remove_outpost_submarine(gameid : int, tick : int, outpostname : String, outposthistory : Array, source : String) -> void:
	remove_outpost_troops(tick, outposthistory, source)
	remove_outpost_shield(tick, outposthistory, source)
	remove_outpost_player(tick, outposthistory, source)
	set_troop_generation(gameid, tick, outpostname, outposthistory)
	set_shield_generation(gameid, tick, outpostname, outposthistory)
	set_outpost_history(gameid, tick, outpostname, outposthistory)

# Generation functions

# Expands histories
func set_history_expansion(gameid : int, starttick : int, endtick : int) -> void:
	var GameData : Dictionary = Games[gameid]
	for outpost in GameData["OutpostData"]:
		set_outpost_history_expansion(gameid, starttick, endtick, outpost["OutpostName"], outpost["History"])
		set_outpost_history(gameid, starttick, outpost["OutpostName"], outpost["History"], false)
	for submarine in GameData["SubmarineData"]:
		if set_submarine_history_expansion(gameid, starttick, endtick, submarine["SubmarineName"], submarine["History"]) != "Failure":
			set_submarine_history(gameid, starttick, submarine["SubmarineName"], submarine["History"], false)
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "set_global_variable", "VisibleTickMax", GameData["VisibleTickMax"])

# Expands outpost history
func set_outpost_history_expansion(gameid : int, starttick : int, endtick : int, outpostname : String, outposthistory : Array) -> void:
	var StartHistoryEntry : Dictionary = outposthistory[-1]
	for i in endtick - StartHistoryEntry["Tick"] - 1:
		# TODO: maybe remove + 1
		var ITickHistory : int = StartHistoryEntry["Tick"] + i + 1
		var NewHistoryEntry : Dictionary = StartHistoryEntry.duplicate(true)
		NewHistoryEntry["Tick"] = ITickHistory
		outposthistory.append(NewHistoryEntry)
	set_shield_generation(gameid, starttick, outpostname, outposthistory)
	set_troop_generation(gameid, starttick, outpostname, outposthistory)

# Expands submarine history
func set_submarine_history_expansion(gameid : int, starttick : int, endtick : int, submarinename : String, submarinehistory : Array) -> String:
	if endtick <= submarinehistory[-1]["Tick"]:
		var InitialOutpost : String = get_data_name(gameid, submarinename)["InitialOutpost"]
		set_submarine_combat(gameid, submarinename, InitialOutpost, submarinehistory)
		set_submarine_arrival(gameid, submarinename, InitialOutpost, submarinehistory)
		return "Success"
	# Failure case
	return "Failure"

# Sets outpost shield generation
func set_shield_generation(gameid : int, tick : int, outpostname : String, outposthistory : Array) -> void:
	var GameData : Dictionary = Games[gameid]
	var TickHistory : int = get_tick_history(tick, outposthistory)
	set_outpost_shield(TickHistory, outposthistory, outpostname, outposthistory[TickHistory]["Shield"][outpostname])
	for i in GameData["VisibleTickMax"] - tick:
		var ITickHistory : int = TickHistory + i
		var HistoryEntry = outposthistory[ITickHistory]
		var CurrentGenerateShieldTicks : int = int(ceil(GameData["GenerateShieldTicks"] * HistoryEntry["ShieldTimeMultiplierTotal"]))
		var CurrentShieldTimeCountdown : int
		if GameData["Players"][HistoryEntry["CurrentPlayer"]]["Username"] == "Empty":
			CurrentShieldTimeCountdown = CurrentGenerateShieldTicks
		else:
			var LoweredShieldTimeCountdown = CurrentGenerateShieldTicks
			if ITickHistory != 0:
				LoweredShieldTimeCountdown = outposthistory[ITickHistory - 1]["ShieldTimeCountdown"] - 1
			CurrentShieldTimeCountdown = LoweredShieldTimeCountdown + CurrentGenerateShieldTicks * int(LoweredShieldTimeCountdown < 0)
		HistoryEntry["ShieldTimeCountdown"] = CurrentShieldTimeCountdown
		if CurrentShieldTimeCountdown == 0:
			var ShieldIncrease : int = GameData["GenerateShieldNumber"] * HistoryEntry["ShieldNumberMultiplierTotal"]
			for secondaryi in len(outposthistory) - TickHistory - i:
				var SecondaryITickHistory : int = TickHistory + i + secondaryi
				var MaximumShieldIncrease : int = min(ShieldIncrease, outposthistory[SecondaryITickHistory]["ShieldMax"] - outposthistory[SecondaryITickHistory]["Shield"][outpostname])
				outposthistory[SecondaryITickHistory]["Shield"][outpostname] += MaximumShieldIncrease
				outposthistory[SecondaryITickHistory]["ShieldTotal"] += MaximumShieldIncrease

# Sets outpost troop generation
func set_troop_generation(gameid : int, tick : int, outpostname : String, outposthistory : Array) -> void:
	var GameData : Dictionary = Games[gameid]
	var TickHistory : int = get_tick_history(tick, outposthistory)
	set_outpost_troops(TickHistory, outposthistory, outpostname, outposthistory[TickHistory]["Troops"][outpostname])
	for i in GameData["VisibleTickMax"] - tick:
		var ITickHistory : int = TickHistory + i
		var HistoryEntry = outposthistory[ITickHistory]
		if HistoryEntry["CurrentType"] == "Factory":
			var CurrentGenerateTroopTicks : int = int(ceil(GameData["GenerateTroopTicks"] * HistoryEntry["TroopTimeMultiplierTotal"]))
			var CurrentTroopTimeCountdown : int
			if GameData["Players"][HistoryEntry["CurrentPlayer"]]["Username"] == "Empty":
				CurrentTroopTimeCountdown = CurrentGenerateTroopTicks
			else:
				var LoweredTroopTimeCountdown = CurrentGenerateTroopTicks
				if ITickHistory != 0:
					LoweredTroopTimeCountdown = outposthistory[ITickHistory - 1]["TroopTimeCountdown"] - 1
				CurrentTroopTimeCountdown = LoweredTroopTimeCountdown + CurrentGenerateTroopTicks * int(LoweredTroopTimeCountdown < 0)
			HistoryEntry["TroopTimeCountdown"] = CurrentTroopTimeCountdown
			if CurrentTroopTimeCountdown == 0:
				var TroopIncrease : int = GameData["GenerateTroopNumber"] * HistoryEntry["TroopNumberMultiplierTotal"]
				for secondaryi in len(outposthistory) - TickHistory - i:
					var SecondaryITickHistory : int = TickHistory + i + secondaryi
					outposthistory[SecondaryITickHistory]["Troops"][outpostname] += TroopIncrease
					outposthistory[SecondaryITickHistory]["TroopTotal"] += TroopIncrease
		else:
			HistoryEntry["TroopTimeCountdown"] = GameData["GenerateTroopTicks"] * HistoryEntry["TroopTimeMultiplierTotal"]

# Remote functions

# Creates submarine
remote func create_submarine(gameid : int, id : int, tick : int, initialoutpost : String, username : String, initialtarget : String) -> void:
	var GameData : Dictionary = Games[gameid]
	var InitialPlayer : String = get_username_color(gameid, username)
	var InitialOutpostPosition : Vector2 = get_data_name(gameid, initialoutpost)["InitialPosition"]
	var SubamrineName : String = get_submarine_name(gameid, initialoutpost)
	var NewData : Dictionary = {
		"SubmarineName" : SubamrineName,
		"History" : get_submarine_history(gameid, tick, SubamrineName, initialoutpost, InitialPlayer, InitialOutpostPosition, initialtarget),
		"InitialOutpost" : initialoutpost
	}
	GameData["SubmarineData"].append(NewData)
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "set_submarine_data", NewData)
	rpc_id(id, "focus_submarine", SubamrineName)

# Removes submarine
remote func remove_submarine(gameid : int, submarinename : String) -> void:
	var GameData : Dictionary = Games[gameid]
	# TODO: check if target is outpost or submarine
	var Data : Dictionary = get_data_name(gameid, submarinename)
	var History : Array = Data["History"]
	var InitialOutpostData = get_data_name(gameid, Data["InitialOutpost"])
	var EndTargetData = get_data_name(gameid, History[-1]["Target"])
	remove_outpost_submarine(gameid, History[0]["Tick"], InitialOutpostData["OutpostName"], InitialOutpostData["History"], submarinename)
	remove_outpost_submarine(gameid, History[0]["Tick"], EndTargetData["OutpostName"], EndTargetData["History"], submarinename)
	var SubmarineData = GameData["SubmarineData"]
	for i in len(SubmarineData):
		if SubmarineData[i]["SubmarineName"] == submarinename:
			SubmarineData.remove(i)
			break
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "remove_submarine", submarinename)

# Changes submarine troops whilst preparing
remote func prepare_submarine_troops(gameid : int, submarinename : String, troops : int) -> void:
	var Data : Dictionary = get_data_name(gameid, submarinename)
	var History : Array = Data["History"]
	var StartTick : int = History[0]["Tick"]
	var InitialOutpostHistory : Array = get_data_name(gameid, Data["InitialOutpost"])["History"]
	set_submarine_troops(StartTick, History, Data["InitialOutpost"], troops)
	set_outpost_troops(StartTick, InitialOutpostHistory, submarinename, -troops)
	set_submarine_combat(gameid, submarinename, Data["InitialOutpost"], History)
	set_submarine_arrival(gameid, submarinename, Data["InitialOutpost"], History)
	set_submarine_history(gameid, StartTick, submarinename, History)
	set_outpost_history(gameid, StartTick, Data["InitialOutpost"], InitialOutpostHistory)

# Sets submarine gift and calculates history
remote func gift_submarine(gameid : int, tick : int, submarinename : String, value : bool) -> void:
	var Data : Dictionary = get_data_name(gameid, submarinename)
	var History : Array = Data["History"]
	set_submarine_gift(tick, History, value)
	set_submarine_combat(gameid, submarinename, Data["InitialOutpost"], History)
	set_submarine_arrival(gameid, submarinename, Data["InitialOutpost"], History)
	set_submarine_history(gameid, tick, submarinename, History)

# Game functions

# Creates a new game
remote func create_game(id : int, gamename : String, maxplayers : int, ticklength : int, username : String) -> void:
	var Players : Dictionary = {Colors["Empty"] : {"Username" : "Empty", "Occupied" : true}}
	var AvailableColors : Dictionary = Colors.duplicate(true)
	AvailableColors.erase("Empty")
	for i in maxplayers:
		var PlayerColorName : String = AvailableColors.keys()[rand_range(0, len(AvailableColors.keys()))]
		var PlayerColor = AvailableColors[PlayerColorName]
		Players[PlayerColor] = {"Username" : PlayerColorName, "Occupied" : false}
		AvailableColors.erase(PlayerColor)
	var NewGame = {
		"GameName" : gamename,
		"OnlinePlayers" : {},
		"MaxPlayers" : maxplayers,
		"GlobalTick" : 0,
		"VisibleTickMax" : 300,
		"Paused" : true,
		"Players" : Players,
		"OutpostData" : [],
		"SubmarineData" : [],
		"TickLength" : ticklength,
		"SubmarineSpeed" : 30,
		"SubmarineWaitModifier" : 5,
		"OutpostStartingTroops" : 40,
		"GenerateTroopTicks" : 20,
		"GenerateShieldTicks" : 20,
		"GenerateTroopNumber" : 6,
		"GenerateShieldNumber" : 1,
	}
	var GameId : int = randi()
	Games[GameId] = NewGame
	generate_random_map(GameId, Players)
	join_game(GameId, id, username)
	var SaveFile = File.new()
	SaveFile.open(get_save_path(), File.WRITE)
	get_node("/root/Game").create_tick_timer(GameId)

# Sends client current game state
remote func open_game(gameid : int, id : int, username : String) -> void:
	var GameData : Dictionary = Games[gameid]
	var UsernameColor : String = get_username_color(gameid, username)
	var OnlinePlayers : Dictionary = GameData["OnlinePlayers"]
	if OnlinePlayers.get(UsernameColor) == null:
		OnlinePlayers[UsernameColor] = {"Ids" : []}
	OnlinePlayers[UsernameColor]["Ids"].append(id)
	var GameStateVariables : Array = [
		["OnlinePlayers", OnlinePlayers],
		["OutpostData", GameData["OutpostData"]],
		["SubmarineData", GameData["SubmarineData"]],
		["GlobalTick", GameData["GlobalTick"]],
		["Paused" , GameData["Paused"]],
		["TickLength", GameData["TickLength"]],
		["SubmarineSpeed", GameData["SubmarineSpeed"]],
		["SubmarineWaitModifier", GameData["SubmarineWaitModifier"]],
		["OutpostStartingTroops", GameData["OutpostStartingTroops"]],
		["GenerateShieldTicks", GameData["GenerateShieldTicks"]],
		["GenerateTroopTicks", GameData["GenerateTroopTicks"]],
		["GenerateTroopNumber", GameData["GenerateTroopNumber"]],
		["GenerateShieldNumber", GameData["GenerateShieldNumber"]],
		["VisibleTickMax", GameData["VisibleTickMax"]],
		["CalculateTroopsDelay", CalculateTroopsDelay],
		["Players", GameData["Players"]]
	]
	print("Sending global variables")
	for variable in GameStateVariables:
		rpc_id(id, "set_global_variable", variable[0], variable[1])
	for playerid in get_online_player_ids(gameid):
		if playerid != id:
			rpc_id(playerid, "add_online_player", UsernameColor, id)
	print("Sending game state updated call")
	rpc_id(id, "game_state_updated")

# Returns the games a username is present in
remote func get_joined_games(id : int, username : String) -> void:
	var GamesInfo : Dictionary = {}
	for gameid in Games:
		if get_username_in_game(gameid, username) == true:
			GamesInfo[gameid] = {"GameName" : Games[gameid]["GameName"]}
	rpc_id(id, "set_joined_games", GamesInfo)

# Returns the games a username is not present in
remote func get_open_games(id : int, username : String) -> void:
	var GamesInfo : Dictionary = {}
	for gameid in Games:
		if get_username_in_game(gameid, username) == false:
			GamesInfo[gameid] = {"GameName" : Games[gameid]["GameName"]}
	rpc_id(id, "set_open_games", GamesInfo)

# Adds a player to a game
func add_joined_game(gameid : int, id : int) -> void:
	var GameData : Dictionary = Games[gameid]
	var GameInfo = {"GameName" : GameData["GameName"]}
	rpc_id(id, "add_joined_game", gameid, GameInfo)

# Checks if a player can join a game and adds them to it
remote func join_game(gameid : int, id : int, username : String) -> void:
	var GameData : Dictionary = Games[gameid]
	var Players : Dictionary = GameData["Players"]
	var UnoccupiedPlayers : Array = []
	for player in Players:
		if Players[player]["Occupied"] == false:
			UnoccupiedPlayers.append(player)
	if len(UnoccupiedPlayers) > 0:
		if check_username(gameid, username):
			add_joined_game(gameid, id)
			var UnoccupiedPlayer : String = UnoccupiedPlayers[rand_range(0, len(UnoccupiedPlayers))]
			var Player : Dictionary = Players[UnoccupiedPlayer]
			Player["Occupied"] = true
			Player["Username"] = username
			for playerid in get_online_player_ids(gameid):
				if playerid != id:
					rpc_id(playerid, "set_player", UnoccupiedPlayer, Player)
			if len(UnoccupiedPlayers) - 1 == 0:
				get_node("/root/Game").start_game(gameid)

# Unpauses the game
func unpause(gameid : int) -> void:
	var GameData = Games[gameid]
	GameData["Paused"] = true
	for playerid in get_online_player_ids(gameid):
		rpc_id(playerid, "set_global_variable", "Paused", false)

# Generates a random map
func generate_random_map(gameid : int, players : Dictionary) -> void:
	var GameData : Dictionary = Games[gameid]
	var Names : Array = [
		"Zochuona",
		"Gewhoagro",
		"Qecren",
		"Rubreau",
		"Bleuvania",
		"Scoaca",
		"Osnines",
		"Abrana",
		"Sheir Grary",
		"Friub Crye",
		"Rubloidor",
		"Veshuiye",
		"Zaplistan",
		"Joprines",
		"Braonga",
		"Shaissau",
		"Espela",
		"Astea",
		"Plaii Skax",
		"Sciyc Chait",
		"Tadruosil",
		"Bagruasau",
		"Doshines",
		"Gogrium",
		"Driuye",
		"Gluadan",
		"Ethana",
		"Uspea",
		"Smauq Stines",
		"Streiv Frein"
	]
	Games[gameid]["OutpostData"] = []
	for i in len(Names):
		var Player : String = players.keys()[rand_range(0, len(players.keys()))]
		var Position : Vector2 = Vector2(-1800, -750) + Vector2((i % 6) * 600, int(floor(i / 6)) * 300) + Vector2(rand_range(-100, 100), rand_range(-50, 50))
		var Troops : int
		if players[Player]["Username"] == "Empty":
			Troops = 0
		else:
			Troops = GameData["OutpostStartingTroops"]
		var Data : Dictionary = {
			"OutpostName" : Names[i],
			"History" : get_outpost_history(gameid, Names[i], Player, "Factory", Troops, 0, 40),
			"InitialPosition" : Position,
			"SubmarineCount" : 0
		}
		Games[gameid]["OutpostData"].append(Data)

# Variable functions

# Returns the name for a new submarine launched from passed outpost
func get_submarine_name(gameid : int, outpostname : String) -> String:
	var Data : Dictionary = get_data_name(gameid, outpostname)
	var SubmarineName : String = str(Data["OutpostName"]) + "-" + str(Data["SubmarineCount"])
	Data["SubmarineCount"] += 1
	return SubmarineName

# Checks if passed username is valid
func check_username(gameid : int, username : String) -> bool:
	var GameData : Dictionary = Games[gameid]
	var Player = GameData["Players"].get(username)
	return Player == null or Player["Occupied"] == false

# Checks if a username is present in a game
func get_username_in_game(gameid : int, username : String) -> bool:
	var GameData : Dictionary = Games[gameid]
	for player in GameData["Players"]:
		if GameData["Players"][player]["Username"] == username:
			return true
	return false

# Returns the username's color
func get_username_color(gameid : int, username : String) -> String:
	var GameData = Games[gameid]
	for player in GameData["Players"]:
		if GameData["Players"][player]["Username"] == username:
			return player
	return "FFFFFF"

# Removes an id from all players variables
func disconnect_player(id : int) -> void:
	for gameid in Games:
		remove_player_id(gameid, id)

# Removes player id from passed game
remote func remove_player_id(gameid : int, id : int) -> void:
	var GameData : Dictionary = Games[gameid]
	var OnlinePlayers : Dictionary = GameData["OnlinePlayers"]
	for player in OnlinePlayers:
		for playerid in OnlinePlayers[player]["Ids"]:
			if playerid == id:
				Games[gameid]["OnlinePlayers"][player]["Ids"].erase(playerid)
			for onlineplayerid in get_online_player_ids(gameid):
				rpc_id(onlineplayerid, "remove_online_player", player, id)
			break

# Returns the values for game setup options
remote func get_game_setup_options(id : int) -> void:
	rpc_id(id, "set_game_setup_options", MaxPlayers, TickLengths)
